import {userConstants} from "../constants";

const initialState = {
    user: {},
    error: null,
    loading: false
};

const registrationReducer = (state=initialState, action) => {
    switch (action.type) {
        case userConstants.REGISTER_LOADING:
            return {
                ...state,
              loading: true
            };
        case userConstants.REGISTER_REQUEST:
            return {
                ...state,
                user: action.payload,
                };
        case userConstants.REGISTER_SUCCESS:
            return {
                error: null
            };
        case userConstants.REGISTER_FAILURE:
            return {
                error: action.payload
            };
        default:
            return state
    }
};
export {registrationReducer};