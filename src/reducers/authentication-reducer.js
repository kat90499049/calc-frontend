import {userConstants} from "../constants";

let user = JSON.parse(localStorage.getItem('user'));
const initialState = user ? {loggedIn: true, user } : {};

export const authenticationReducer = (state = initialState, action) => {
    switch (action.type) {
        case userConstants.LOGIN_LOADING:
            return {
                loading: true
            };
        case userConstants.LOGIN_SUCCESS:
            return {
                loggedIn: true,
                user: action.user
            };
        case userConstants.LOGIN_FAILURE:
            return {
                error:action.payload
            };
        case userConstants.LOGOUT:
            return {};
        default:
            return state
    }
};