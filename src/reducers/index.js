import { combineReducers} from "redux";

import {registrationReducer} from "./registration-reducer";
import {alert} from "./alert-reducer";
import {authenticationReducer} from "./authentication-reducer";
import {userReducer} from "./user-reducer";

const rootReducer = combineReducers({
    registrationReducer,
    alert,
    authenticationReducer,
    userReducer
});

export default rootReducer;
