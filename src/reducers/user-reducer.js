import {userConstants} from "../constants";

let user = JSON.parse(localStorage.getItem('user'));
const initialState = user ? {loggedIn: true, user } : {};

export const userReducer = (state = initialState, action)=> {
    switch (action.type) {
        case userConstants.ADD_CATEGORIES_OUTCOME:
            return{
                user: action.user
            };
        case userConstants.ADD_CATEGORIES_INCOME:
            return  {
                user: action.user
            };
        case userConstants.USER_LOADING:
            return {
                loading: action.payload
            };
        default:
            return state
    }
};