import {backurl} from '../utils/config';
import axios from "axios";
import {
    loginUserFailure,
    loginUserLoading,
    loginUserSuccess, userLoading,
    userRegisterFailure,
    userRegisterLoading,
    userRegisterRequest,
    userRegisterSuccess
} from "../actions/user-actions";
import {history} from "../history";

export const registerUser = (user) => async (dispatch) => {

    try {
        dispatch(userRegisterLoading());
        let res= await axios.post(`${backurl}/registration`, user);

        localStorage.setItem("token", res.data.data.token);

        dispatch(userRegisterSuccess(user));
        history.push('/home')
    } catch (e) {
        dispatch(userRegisterFailure(e.response.data.message))
    }
};

export const loginUser = (email, password) =>async (dispatch) => {

    try {
        dispatch(loginUserLoading());
        let res = await axios.post(`${backurl}/login`, {email, password});

        localStorage.setItem("token", res.data.data.token);
        dispatch(loginUserSuccess());
        history.push('/home');
    } catch (e) {
        dispatch(loginUserFailure(e.response.data.message))
    }
};

export const userDetail = () => async (dispatch) => {

    const token = localStorage.getItem("token");

    if (token) {
        try{
            let res = await axios.get(`${backurl}/details`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });
            dispatch(userRegisterRequest(res.data.data))
        } catch (e) {
            history.push('/login')
        }
    } else {
        history.push('/login')
    }
};

export const addCategoryIncome = (category) => async (dispatch) => {

    const token = localStorage.getItem("token");

    if (token) {
        let res = await axios.post(`${backurl}/categories-income`, {"name":category}, {
            headers: {
                'Authorization': `Bearer ${token}`
        }
        });

        dispatch(userRegisterRequest(res.data.user))
    } else {
        history.push('/login')
    }
};

export const addCategoryOutcome = (category) => async (dispatch) => {

    const token = localStorage.getItem("token");

    if (token) {
        dispatch(userLoading(true));
        let res = await axios.post(`${backurl}/categories-outcome`, {"name":category}, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        dispatch(userRegisterRequest(res.data.user));
        dispatch(userLoading(false));
    } else {
        history.push('/login')
    }
};

export const deleteCategoryOutcome = (id) => async (dispatch) => {

    const token = localStorage.getItem("token");

    if (token) {
        dispatch(userLoading(true));
        let res = await axios.delete(`${backurl}/categories-outcome/${id}`,
            {headers: {
                'Authorization': `Bearer ${token}`
            }
            });

        dispatch(userRegisterRequest(res.data.user));
        dispatch(userLoading(false));
    } else {
        history.push('/login')
    }


};

export const addBill = (bill) => async (dispatch) => {

    const token = localStorage.getItem("token");

    if (token) {
        dispatch(userLoading(true));
        let res = await axios.post(`${backurl}/bills  `, {"name":bill}, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        dispatch(userRegisterRequest(res.data.user));
        dispatch(userLoading(false));
    } else {
        history.push('/login')
    }
};

export const deleteBill = (id) => async (dispatch) => {

    const token = localStorage.getItem("token");

    if (token) {
        dispatch(userLoading(true));
        let res = await axios.delete(`${backurl}/bills/${id}`,
            {headers: {
                    'Authorization': `Bearer ${token}`
                }
            });

        dispatch(userRegisterRequest(res.data.user));
        dispatch(userLoading(false));
    } else {
        history.push('/login')
    }


};

export const editBill = (id, item) => async (dispatch) => {

    const token = localStorage.getItem("token");

    if (token) {
        dispatch(userLoading(true));
        let res = await axios.put(`${backurl}/bills/${id}`, {name: item},
            {headers: {
                    'Authorization': `Bearer ${token}`
                }
            });

        dispatch(userRegisterRequest(res.data.user));
        dispatch(userLoading(false));
    } else {
        history.push('/login')
    }


};

export const addIncome = (income) => async (dispatch) => {

    const token = localStorage.getItem("token");

    if(token) {
        dispatch(userLoading(true));
        let res = await axios.post(`${backurl}/incomes`, income, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        dispatch(userRegisterRequest(res.data.user));
        dispatch(userLoading(false));
    }

};

export const addOutcome = (outcome) => async (dispatch) => {

    const token = localStorage.getItem("token");

    if(token) {
        dispatch(userLoading(true));
        let res = await axios.post(`${backurl}/outcomes`, outcome, {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        });

        dispatch(userRegisterRequest(res.data.user));
        dispatch(userLoading(false));
    }

};

