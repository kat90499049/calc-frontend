import {userConstants} from "../constants";

export const userRegisterLoading = () => {
    return {
        type: userConstants.REGISTER_LOADING,
    }
};

export const userRegisterRequest = (user) => {
    return {
    type: userConstants.REGISTER_REQUEST,
    payload: user
    }
};

export const userRegisterSuccess = () => {
    return {
        type: userConstants.REGISTER_SUCCESS
    }
};

export const userRegisterFailure = (error) => {
    return {
        type: userConstants.REGISTER_FAILURE,
        payload: error
    }
};

export const loginUserLoading = () => {
    return {
        type: userConstants.LOGIN_LOADING,
    }
};

export const loginUserSuccess =(user) => {
    return {
        type: userConstants.LOGIN_SUCCESS,
        payload: user
    }
};

export const loginUserFailure =(error) => {
    return {
        type: userConstants.LOGIN_FAILURE,
        payload: error
    }
};

export const logoutUser = ()=> {
    return {
        type: userConstants.LOGOUT
    }
};

export const userLoading = (loading) => {
    return {
        type: userConstants.USER_LOADING,
        payload: loading
    }
};

// export const addCategoriesOutcome = ()=> {
//     return{
//         type: userConstants.ADD_CATEGORIES_OUTCOME
//     }
// };
