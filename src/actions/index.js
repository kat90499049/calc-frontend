
import {alertActions} from "./alert-actions";
import * as userActions from "./user-actions";

export {
    userActions,
    alertActions
}

// {
//     userRegisterLoading,
//         userRegisterRequest,
//         userRegisterSuccess,
//         userRegisterFailure,
//         loginUserLoading,
//         loginUserRequest,
//         loginUserSuccess,
//         loginUserFailure,
//         logoutUser
// }