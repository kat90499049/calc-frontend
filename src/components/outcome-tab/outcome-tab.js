import React,{useState} from "react";

import {useDispatch, useSelector} from "react-redux";
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import brown from "@material-ui/core/colors/brown";
import Box from "@material-ui/core/Box";
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import {red} from "@material-ui/core/colors";
import { Input } from 'semantic-ui-react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import moment from "moment";
import MomentUtils from '@date-io/moment';
import {
    MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import { KeyboardDatePicker } from "@material-ui/pickers";
import "moment/locale/ru";
import { Button} from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import {addCategoryOutcome, addOutcome} from "../../services/service";
import InputSum from "../input-sum";
import AddBills from "../add-bills";
import FormHelperText from '@material-ui/core/FormHelperText';
import './outcome-tab.scss';

const useStyles = makeStyles((theme) => ({
    paper: {
        minWidth: 200,
        marginRight: 10,
        [theme.breakpoints.down('xs')]: {
            minWidth: 100,
            width: 150
        },
        [theme.breakpoints.down(500)]: {
            width: 100
        },
    },
    add: {
        color: brown[900],
        marginTop: 15
    },
    left: {
        color: red[400],
        marginRight: 10,
        [theme.breakpoints.down('xs')]: {
            marginRight: 5,
        },
    },
    leftIcon: {
        fontSize: 70,
        [theme.breakpoints.down('xs')]: {
            fontSize: 50,
        },
    },
    dataLabel: {
        [theme.breakpoints.down(500)]: {
            marginRight: 13,
        },
    },
    dataRadio: {
        [theme.breakpoints.down(500)]: {
            padding: 0
        },
    }
}));

moment.locale("ru");

export default function OutcomeTab({handleClose}) {

    const dispatch = useDispatch();

    const classes = useStyles();

    // const loading = useSelector((state)=> state.userReducer.loading);

    const userLoading = useSelector((state) => state.registrationReducer.user);

    const [deactivated, setDeactivated] = useState(false);

    const [categoryOutcomeValue, setCategoryOutcomeValue] = useState();

    const [bill, setBill] = useState('');

    const [indicateData, setIndicateData]=useState(false);

    const [inputSum, setInputSum] = useState('');

    const today = moment();
    const yesterday=moment().subtract(1, "days");

    const [value, setValue] = useState(today.format("YYYY/MM/DD"));
    const [selectedDate, setDateChange] = useState(today.format("YYYY/MM/DD"));

    const [categorySubmit, setCategorySubmit] = useState('');

    const [submitted, setSubmitted] = useState(false);

    const [error, setError] = useState(false);

    const [errorInputSum, setErrorInputSum] = useState('');

    const handleSubmitCategory = (e) => {
        e.preventDefault();
        setCategorySubmit(e.target.value)
    };

    const handleChangeData = (event) => {
        setValue(event.target.value);
        if(event.target.value==="other"){
            setIndicateData(true);
            setDateChange(today.format("YYYY/MM/DD"));
        } else {
            setIndicateData(false);
        }
    };

    const handleChange = (event) => {
        setBill(event.target.value);
        if(event.target.value !== ''){
            setError(false);
        }
    };
    const handleAddCategory = (e) => {
        e.preventDefault();
        dispatch(addCategoryOutcome(categoryOutcomeValue));
        handleActivate(e);
    };

    const handleChangeCategoryOutcomeValue = ({target: {value}})=> {
        setCategoryOutcomeValue(value)
    };

    const handleActivate = (e) => {
        e.preventDefault();
        if(!deactivated) {
            setDeactivated(true);
        } else {
            setDeactivated(false)
        }
    };

    const closeButton=(e)=> {
        e.preventDefault();
        handleClose();
    };

    const handleSubmit = (e) => {

        e.preventDefault();

        let date = e.target.date_outcome.value;

        if(e.target.date_outcome.value === 'other'){
            date = moment(selectedDate).format("YYYY/MM/DD")
        }

        if(e.target.bill.value === ''){
            setError(true)
        } else {
            setError(false)
        }
        if(e.target.numberformat.value === ''){
            setErrorInputSum('Необходимо заполнить сумму.')
        }

        let sum = e.target.numberformat.value;
        if(sum !== ''){
            sum = e.target.numberformat.value.replace(/,/g, '').match( /[+-]?\d+(\.\d+)?/g)[0]
        }
        const submitData = {
            category_id: categorySubmit,
            sum: sum,
            bill_id: e.target.bill.value,
            date: date
        };
        setSubmitted(true);
        if( sum !== '' && categorySubmit !== '' && e.target.bill.value !== ''){
            dispatch(addOutcome(submitData));
            handleClose();
        }

    };

    return (
        <form
            className="outcome-tab"
            onSubmit={(event)=>handleSubmit(event)}
        >
            <div className="row outcome-tab_sum-bill">
                <InputSum
                    changeSum={(inputSum)=>setInputSum(inputSum)}
                    errorInputSum={errorInputSum}
                    setErrorInputSum={setErrorInputSum}
                />
                <Box className={classes.left}>
                    <ChevronLeftIcon className={classes.leftIcon}/>
                </Box>
                <FormControl
                    variant="outlined"
                    className={classes.paper}
                    error={error}
                >
                    <InputLabel id="demo-simple-select-outlined-label">Счет</InputLabel>
                    <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        value={bill}
                        onChange={handleChange}
                        label="Bill"
                        name="bill"
                    >
                        {
                            userLoading?.bills ?
                                userLoading?.bills.map((bill)=> {
                                    return (
                                        <MenuItem key={bill.id} value={bill.id}>{bill.name}</MenuItem>
                                    )
                                })
                                : null
                        }
                    </Select>
                    {submitted && !bill &&
                    <FormHelperText>Необходимо выбрать счет</FormHelperText>
                    }
                </FormControl>
            </div>
            <div className="row outcome-tab_add-bill">
                <AddBills/>
            </div>
            <div className="row outcome-tab_category-name">
                <h3>Выберите катергорию</h3>
            </div>
            <div className="row outcome-tab_category-button">
                {
                    userLoading?.categories_outcome ?
                        userLoading?.categories_outcome.map((category) => {
                            return (
                                <Button key={category.id} inverted color='red'
                                        className={category.id==categorySubmit? 'active': ''}
                                        value={category.id}
                                        onClick={(event => handleSubmitCategory(event))}
                                >
                                    {category.name}
                                </Button>
                            )})

                        : null
                }
                {
                    !deactivated &&


                    <Button inverted color='orange' id="addButton"
                            onClick={(event => handleActivate(event))}
                    >
                        Добавить категорию
                        <AddCircleIcon />
                    </Button>

                }

                {deactivated && userLoading?
                    <div className="outcome-tab_input" >
                        <Input placeholder='категория'
                               name="category_outcome"
                               onChange={(event => handleChangeCategoryOutcomeValue(event))}
                        />
                        <Button.Group>
                            <Button color='green' icon='chevron circle down'
                                    onClick={(event => handleAddCategory(event))}
                            />
                            <Button color='red' icon='times circle'
                                    onClick={(event => handleActivate(event))}/>
                        </Button.Group>
                    </div>
                    : null
                }

            </div>
            {
                submitted && !categorySubmit &&
                <div className="row outcome-tab_category-helpertext">Необходимо выбрать категорию.</div>
            }
            <div className="row outcome-tab_data">
                <FormControl component="fieldset">
                    <FormLabel component="legend">Когда:</FormLabel>
                    <RadioGroup row aria-label="data-outcome" name="date_outcome" value={value} onChange={(event)=>handleChangeData(event)}>
                        <FormControlLabel className={classes.dataLabel} value={yesterday.format("YYYY/MM/DD")} control={<Radio className={classes.dataRadio}/>} label="Вчера" />
                        <FormControlLabel className={classes.dataLabel} value={today.format("YYYY/MM/DD")} control={<Radio className={classes.dataRadio}/>} label="Сегодня" />
                        <FormControlLabel className={classes.dataLabel} value="other" control={<Radio className={classes.dataRadio}/>} label="Указать дату"/>
                    </RadioGroup>
                </FormControl>
            </div>
            {
                indicateData?
                    <div className="row outcome-tab_data-input">
                        <div className="naming">Указать дату:   </div>
                        <MuiPickersUtilsProvider utils={MomentUtils}>
                            <KeyboardDatePicker
                                name="date_other"
                                clearable
                                value={selectedDate}
                                placeholder="2018/10/10"
                                onChange={date => setDateChange(date)}
                                format="YYYY/MM/DD"
                            />
                        </MuiPickersUtilsProvider>
                    </div>
                    : null
            }

            <div className="row outcome-tab_send-buttons">
                <Button color='yellow'
                        onClick={closeButton}
                >
                    Отмена
                </Button>
                <Button type="submit"
                        color='olive'
                >
                    Сохранить
                </Button>
            </div>

        </form>
    )
}