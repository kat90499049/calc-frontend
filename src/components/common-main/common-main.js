import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Box from '@material-ui/core/Box';
import {useDispatch, useSelector} from "react-redux";
import AddBills from "../add-bills";
import {brown, green, yellow} from "@material-ui/core/colors";
import './common-main.scss';
import moment from "moment";
import AddCircleIcon from '@material-ui/icons/AddCircle';
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import {deleteBill, editBill} from "../../services/service";
import Backdrop from "@material-ui/core/Backdrop/Backdrop";
import Fade from "@material-ui/core/Fade/Fade";
import {Button, Input} from "semantic-ui-react";
import Modal from "@material-ui/core/Modal";
import ModalPlus from "../modal-plus";
import ModalMinus from "../modal-minus";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: 400,
        backgroundColor: green[50],
    },
    common: {
        backgroundColor: yellow[600]
    },
    icons: {
        padding: 4
    },
    sum: {
        color: brown[900],
        fontSize: 20
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
}));

moment.locale("ru");

export default function CommonMain() {

    const dispatch = useDispatch();

    const classes = useStyles();

    const userLoading = useSelector((state) => state.registrationReducer.user);

    const [open, setOpen] = useState(false);

    const [deletedBill, setDeletedBill] = useState();

    const [openModalPlus, setOpenModalPlus] = useState(false);

    const [openModalMinus, setOpenModalMinus] = useState(false);

    const [activeEdit, setActiveEdit] = useState('');

    const [editBillValue, setEditBillValue]=useState('');

    const today = moment().format('LL');

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleDeletedBill= (bill) => {
        handleOpen();
        setDeletedBill(bill.id)
    };

    const handleDeleteBill = ()=> {
        dispatch(deleteBill(deletedBill));
        handleClose();
    };

    const handleOpenModalPlus = (e)=> {
        e.preventDefault();
        setOpenModalPlus(true);
    };

    const handleOpenModalMinus = (e)=> {
        e.preventDefault();
        setOpenModalMinus(true);
    };

    const handleActiveEdit =(id)=> {
            setActiveEdit(id);
            if(activeEdit!==''){
                setActiveEdit('')
            }
    };
    const handleBillValue=({target:{value}})=> {
        setEditBillValue(value);
    };

    const handleChangeBillValue = (e)=> {
        e.preventDefault();
        dispatch(editBill(activeEdit, editBillValue));
        handleActiveEdit();
    };

    return (
        <div className="common-main">
            <div className="common-main_header">
                <div className="row all">
                    <h3>Остатки</h3>
                </div>
                <div className="row available">
                    <div><h4>Доступно</h4></div>
                    <div><h5>{today}</h5></div>
                    <div>
                        {
                            userLoading?.sum ?
                                <h3>{userLoading.sum} BYN</h3>
                                : null
                        }
                    </div>
                </div>
            </div>
            <div className="common-main_details">
                <div className="row details-header">
                    <h4>Детализация по счетам:</h4>
                </div>
                <div className="debts">

                </div>
                <div className="bills">
                    <h5>Счета</h5>
                    <div className={classes.root}>
                        <List style={{padding: 0}} component="nav" aria-label="main mailbox folders">
                            <Divider/>
                            {
                                userLoading?.bills ?
                                    userLoading?.bills.map((bill)=> {
                                        return (
                                            <Box key={bill.id}>

                                                <ListItem button>

                                                    {activeEdit!==bill.id &&
                                                        <ListItemText primary={bill.name} />
                                                    }
                                                    {
                                                        activeEdit===bill.id &&
                                                        <div className="row bills-edit" >
                                                            <Input
                                                                defaultValue={bill.name}
                                                                placeholder={bill.name}
                                                                   name="bill-edit"
                                                                   onChange={(event => handleBillValue(event))}
                                                            />
                                                            <Button.Group>
                                                                <Button color='green' icon='chevron circle down'
                                                                        onClick={(event => handleChangeBillValue(event))}
                                                                />
                                                                <Button color='red' icon='times circle'
                                                                        onClick={(event => handleActiveEdit(event))}
                                                                />
                                                            </Button.Group>
                                                        </div>
                                                    }

                                                    <Box display="flex" justifyContent="flex-end">
                                                        <ListItemText className={classes.sum} edge="end" primary={`${bill.sum} BYN`}/>
                                                    </Box>
                                                    <IconButton
                                                        onClick={handleOpenModalPlus}
                                                        aria-label="delete"
                                                        className={classes.icons}>
                                                        <AddCircleIcon fontSize="small"/>
                                                    </IconButton>
                                                    <IconButton
                                                        onClick={handleOpenModalMinus}
                                                        aria-label="delete"
                                                        className={classes.icons}>
                                                        <RemoveCircleIcon fontSize="small"/>
                                                    </IconButton>
                                                    <IconButton
                                                        onClick={()=>handleActiveEdit(bill.id)}
                                                        aria-label="delete"
                                                        className={classes.icons}>
                                                        <EditIcon fontSize="small"/>
                                                    </IconButton>
                                                    <IconButton
                                                        onClick={()=>handleDeletedBill(bill)}
                                                        aria-label="delete"
                                                        className={classes.icons}>
                                                        <DeleteIcon fontSize="small"/>
                                                    </IconButton>
                                                </ListItem>
                                                <Divider/>
                                            </Box>
                                        )
                                    })
                                    :null
                            }
                        </List>
                        <ModalPlus
                            openModalPlus={openModalPlus}
                            setOpenModalPlus={setOpenModalPlus}
                        />
                        <ModalMinus
                            openModalMinus={openModalMinus}
                            setOpenModalMinus={setOpenModalMinus}
                        />
                        <Modal
                            aria-labelledby="transition-modal-title"
                            aria-describedby="transition-modal-description"
                            className={classes.modal}
                            open={open}
                            onClose={handleClose}
                            closeAfterTransition
                            BackdropComponent={Backdrop}
                            BackdropProps={{
                                timeout: 500,
                            }}
                        >
                            <Fade in={open}>
                                <div className={classes.paper}>
                                    <h4 id="transition-modal-title">Вы действительно хотите удалить счет?</h4>
                                    <div id="transition-modal-description">
                                        <Button
                                            onClick={handleClose}
                                            variant="contained">Отмена</Button>
                                        <Button
                                            onClick={(event => handleDeleteBill(event))}
                                            variant="contained"
                                            color="red"
                                        >
                                            Удалить
                                        </Button>
                                    </div>
                                </div>
                            </Fade>
                        </Modal>
                    </div>
                    <AddBills/>
                </div>

            </div>
        </div>

    );
}