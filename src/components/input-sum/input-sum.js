import React,{useState} from "react";
import TextField from "@material-ui/core/TextField/TextField";
import {makeStyles} from "@material-ui/core/styles";
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format';

import './input-sum.scss'

const useStyles = makeStyles((theme) => ({
    paper: {
        minWidth: 200,
        marginRight: 10,
        [theme.breakpoints.down('xs')]: {
            minWidth: 100,
            width: 150
        },
        [theme.breakpoints.down(500)]: {
            width: 100
        },
    },
}));

function NumberFormatCustom(props) {
    const { inputRef, onChange, ...other } = props;
    const formatter = new Intl.NumberFormat('en-US',
    //     {
    //     style: 'currency',
    //     currency: 'BYN',
    // }
    );

    return (
        <NumberFormat
            {...other}
            getInputRef={inputRef}
            onValueChange={(values) => {
                onChange({
                    target: {
                        name: props.name,
                        value: formatter.format(+values.value),
                    },
                });
            }}
            thousandSeparator
            isNumericString
            // prefix="BYN  "

        />
    );
}

NumberFormatCustom.propTypes = {
    inputRef: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
};

export default function InputSum({changeSum, errorInputSum, setErrorInputSum}) {

    const [values, setValues] = useState({
        numberformat: '',
    });

    const handleChange = (event) => {
        setValues({
            ...values,
            [event.target.name]: event.target.value,
        });
        changeSum(event.target.value);
        if(event.target.value){
            setErrorInputSum('')
        }
    };
    const classes = useStyles();

    return (
        <div className="input-sum">
            <TextField
                error={!!errorInputSum}
                helperText={errorInputSum}
                className={classes.paper}
                id="formatted-numberformat-input"
                label="Сумма"
                variant="outlined"
                value={values.numberformat}
                onChange={handleChange}
                name="numberformat"
                InputProps={{
                    inputComponent: NumberFormatCustom,
                }}
            />
        </div>
    )
}