import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import Box from "@material-ui/core/Box";
import {useDispatch, useSelector} from "react-redux";
import {Button, Input} from "semantic-ui-react";
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';

import './outcome-list.scss';
import {addCategoryOutcome, deleteCategoryOutcome} from "../../services/service";
import {brown} from "@material-ui/core/colors";
import CommonModal from "../common-modal";
import {Link} from "react-router-dom";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    delete: {
        fontSize: 15
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
    addIcon: {
        color: brown[800]
    },
    items: {
        display: 'flex',
        justifyContent: 'space-between'
    }

}));

export default function OutcomeList() {

    const dispatch = useDispatch();

    const classes = useStyles();

    const user = useSelector((state) => state.registrationReducer.user);

    const [activated, setActivated] = useState(false);

    const [open, setOpen] = useState(false);

    const [categoryOutcomeValue, setCategoryOutcomeValue] = useState();

    const [deletedOutcomeCategory, setDeleteOutcomeCategory] = useState();

    // const [openModal, setOpenModal] = useState(false);


    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleAddCategory = (e)=> {
        e.preventDefault();
        dispatch(addCategoryOutcome(categoryOutcomeValue));
        setActivated(false)
    };

    const handleChangeCategoryOutcomeValue = ({target:{value}})=> {
        setCategoryOutcomeValue(value)
    };

    const handleActivate = (e) => {
        e.preventDefault();
        if(!activated) {
            setActivated(true);
        } else {
            setActivated(false)
        }

    };

    const handleDeleteOutcomeCategory = ()=> {
        dispatch(deleteCategoryOutcome(deletedOutcomeCategory));
        handleClose();
    };

    const handleDeletedCategory= (category) => {
        handleOpen();
        setDeleteOutcomeCategory(category.id)
    };

    // const openCommonModal=(e)=> {
    //     e.preventDefault();
    //    setOpenModal(true);
    // };


    return (
        <div className={classes.root}>
            <Grid>
                <Typography component={'div'} variant="h6" className={classes.title}>
                      Категории расходов
                </Typography>
                {/*<CommonModal*/}
                {/*    openModal={openModal}*/}
                {/*    setOpenModal={setOpenModal}*/}
                {/*/>*/}
                <List component="nav" aria-label="main mailbox folders">
                    {
                        user?.categories_outcome ?
                            user?.categories_outcome.map((category) => {
                                return (
                                    <Box key={category.id}>
                                            <ListItem button className={classes.items}>
                                                <Box>
                                                    <Link to="/commoninfo">
                                                        <ListItemText primary={category.name} />
                                                    </Link>
                                                </Box>
                                                <Box >
                                                    <Tooltip title="Удалить" >
                                                        <IconButton

                                                            onClick={()=>handleDeletedCategory(category)}
                                                            style={{padding: 0}}
                                                            aria-label="delete"
                                                        >
                                                            <DeleteIcon />
                                                        </IconButton>
                                                    </Tooltip>
                                                </Box>
                                            </ListItem>

                                        <Divider/>
                                    </Box>
                        )
                        })
                        : null
                     }
                    {
                        !activated &&
                        <ListItem button
                                  onClick={(event => handleActivate(event))}>
                            <ListItemIcon>
                                <AddCircleIcon className={classes.addIcon}/>
                            </ListItemIcon>
                            <ListItemText primary="Добавить категорию" />
                        </ListItem>
                    }
                    {
                        activated &&
                        <div className="outcome_input" >
                            <Input placeholder='категория'
                                   name="category_outcome"
                                   onChange={(event => handleChangeCategoryOutcomeValue(event))}
                            />
                            <Button.Group>
                                <Button color='green' icon='chevron circle down'
                                        onClick={(event => handleAddCategory(event))}
                                />
                                <Button color='red' icon='times circle'
                                        onClick={(event => handleActivate(event))}/>
                            </Button.Group>
                        </div>
                    }

                </List>
            </Grid>
            <Divider />
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className={classes.paper}>
                        <h4 id="transition-modal-title">Вы действительно хотите удалить категорию?</h4>
                        <div id="transition-modal-description">
                            <Button
                                onClick={handleClose}
                                variant="contained">Отмена</Button>
                            <Button
                                onClick={(event => handleDeleteOutcomeCategory(event))}
                                variant="contained"
                                color="red"
                            >
                                Удалить
                            </Button>
                        </div>
                    </div>
                </Fade>
            </Modal>
        </div>
    );
}