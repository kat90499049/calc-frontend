import React, {useState} from "react";
import {Link} from "react-router-dom";
import { useDispatch, useSelector} from 'react-redux';
import { registerUser} from "../../services/service";


import "./register-page.scss";
import BackdropSpinner from "../backdrop-spinner";


const RegisterPage =()=> {

    // componentDidMount() {
    //     this.props.gg()
    // }

    // constructor(props) {
    //     super(props);
    //
    //     // reset login status
    //     // this.props.logout();
    //
    //     this.state = {
    //         user: {
    //             first_name:'',
    //             last_name: '',
    //             email: '',
    //             password: '',
    //         },
    //         submitted:
    //     };
    //
    //
    // }

    const dispatch = useDispatch();

    const [user, setUser] = useState(
        {
            first_name:'',
            last_name: '',
            email: '',
            password: '',
            password_confirmation: '',
            categories_outcome: [],
            categories_income: [],
            bills: [],
            sum: '',
        }
    );


    const error = useSelector((state) => state.registrationReducer.error);

    const loading = useSelector((state) => state.registrationReducer.loading);

    const [submitted, setSubmitted] = useState (false);

    const handleChange=(e)=> {
        const { name, value } = e.target;
        setUser({
                ...user,
                [name]: value

        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        setSubmitted( true );
        if (user.first_name && user.last_name && user.email && user.password && user.password_confirmation) {
            dispatch(registerUser(user));
        }
        // else {
        //     dispatch(registerUserError())
        // }
    };


        // const { registering } = this.props;
        // const { user, submitted } = this.state;
        // const { number} = this.props;
        //
        return (
            <div className="register-page container">
                <div className="row ">
                    <div className="register-page_label col-lg-4 col-md-12 ">
                        <div className="shadow-lg p-3 mb-5 bg-white rounded">
                            <img src="/images/logo.png" alt="Counting House"/>
                        </div>

                    </div>
                    <div className="register-page_form col-lg col-md-12 ">
                        <h2>Регистрация</h2>
                        {/*{userRegisted?.email}*/}
                        {/*<h1>{number}</h1>*/}
                        <form name="form">
                            <div className={'form-group' + (submitted && !user.first_name ? ' has-error' : '')}>
                                <label htmlFor="first_name">Имя</label>
                                <input type="text" className="form-control" name="first_name" value={user.first_name} onChange={(event)=>handleChange(event)} />
                                {submitted && !user.first_name &&
                                <div className="help-block">Необходимо заполнить "Имя"</div>
                                }
                            </div>
                            <div className={'form-group' + (submitted && !user.last_name ? ' has-error' : '')}>
                                <label htmlFor="first_name">Фамилия</label>
                                <input type="text" className="form-control" name="last_name" value={user.last_name} onChange={(event)=>handleChange(event)} />
                                {submitted && !user.last_name &&
                                <div className="help-block">Необходимо заполнить "Фамилия"</div>
                                }
                            </div>
                            <div className={'form-group' + (submitted && !user.email ? ' has-error' : '')}>
                                <label htmlFor="email">E-mail</label>
                                <input type="email" className="form-control" name="email" value={user.email} onChange={(event)=>handleChange(event)} />
                                {submitted && !user.email &&
                                <div className="help-block">Необходимо заполнить "E-mail"</div>
                                }
                            </div>
                            <div className={'form-group' + (submitted && !user.password ? ' has-error' : '')}>
                                <label htmlFor="password">Пароль</label>
                                <input type="password" className="form-control" name="password" value={user.password} onChange={(event)=>handleChange(event)} />
                                {submitted && !user.password &&
                                <div className="help-block">Необходимо заполнить "Пароль"</div>
                                }
                            </div>
                            <div className={'form-group' + (submitted && !user.password_confirmation ? ' has-error' : '')}>
                                <label htmlFor="password-confirmation">Подтверждение пароля</label>
                                <input type="password" className="form-control" name="password_confirmation" value={user.password_confirmation} onChange={(event)=>handleChange(event)} />
                                {submitted && !user.password_confirmation &&
                                <div className="help-block">Необходимо заполнить "Подтверждение пароля"</div>
                                }
                            </div>
                            <div className="form-group form_buttons ">
                                <button type="button" className="btn btn-lg btn-warning"
                                        onClick={(event)=>handleSubmit(event)}
                                        >
                                    Регистрация</button>
                                {/*{loading ? <div className="spinner-border" role="status">*/}
                                {/*    <span className="sr-only">Loading...</span>*/}
                                {/*</div> : <div className="spinner-border-null" role="status">*/}
                                {/*    <span className="sr-only">Loading...</span>*/}
                                {/*</div>*/}
                                {/*}*/}
                                {loading ? <BackdropSpinner/>: null}
                                <button type="button" className="btn btn-lg btn-warning" ><Link to="/">Отмена</Link></button>
                            </div>
                            <div className="form-group form-link">
                                <Link to="/login">Вход в аккаунт</Link>
                                <i className="fa fa-long-arrow-right"></i>
                            </div>
                            {
                                error &&
                                <div className="alert alert-danger" role="alert">
                                    {error}
                                </div>
                            }

                        </form>
                    </div>
                </div>
            </div>
        )
};

export default RegisterPage;