import React from "react";
import {Link} from "react-router-dom";


import "./home-page.scss";
const HomePage = () => {
    return (
        <div className="home-page container">
            <div className="home-page_label">
                <img src="/images/logo.png" alt="Counting House"/>
            </div>
            <div className="home-page_logo">
                <h1>Ваша домашняя <br/> бухгалтерия</h1>
            </div>
            <div className="home-page_article">
                <h4>
                    Добро пожаловать в приложение для ведения домашней бухгалтерии! <br/>
                    Отслеживание расходов, доходов, кредитов и долгов,<br/>
                    всегда под рукой, где бы вы ни находились!
                </h4>
                <h3>
                    Чтобы продолжить работу в приложении <br/>
                    <Link to="/login">войдите в свой аккаунт</Link> или пройдите <Link to="/registration">регистрацию</Link>.
                </h3>
            </div>

            <div className="home-page_buttons">
                <button type="button" className="btn btn-lg btn-warning" ><Link to="/login">Войти в аккаунт</Link></button>
                <button type="button" className="btn btn-lg btn-warning"><Link to="/registration">Регистрация</Link></button>

            </div>


            <div className="home-page_footer">
                <h5>
                    &#169; 2020 Counting House.
                </h5>
            </div>


        </div>
    )
};


export default HomePage;