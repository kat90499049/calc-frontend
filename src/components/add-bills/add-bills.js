import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {makeStyles} from "@material-ui/core/styles";
import brown from "@material-ui/core/colors/brown";
import Button from '@material-ui/core/Button';
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop/Backdrop";
import Fade from "@material-ui/core/Fade/Fade";
import TextField from "@material-ui/core/TextField/TextField";
import {addBill} from "../../services/service";
import AddCircleIcon from '@material-ui/icons/AddCircle';
import './add-bills.scss'
import ListItemText from "@material-ui/core/ListItemText/ListItemText";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon/ListItemIcon";

const useStyles = makeStyles((theme) => ({
    root: {
    },
    add: {
        color: brown[900],
        marginTop: 15
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
    input: {
        marginBottom: 10,
    },
    addIcon: {
    color: brown[800]
    }
}));

export default function AddBills() {

    const dispatch = useDispatch();

    const classes = useStyles();

    const [open, setOpen] = useState(false);

    const [billValue, setBillValue] = useState('');

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleAddBill = (e)=> {
        e.preventDefault();
        if(billValue!==''){
            dispatch(addBill(billValue));
        };
        setBillValue('');
        setOpen(false)
    };

    const handleChangeBillValue = ({target:{value}})=> {
        setBillValue(value)
    };

    const handleActivateModal =(e)=> {
        handleOpen();
    };

    return (
        <div className="add-bills">
            <ListItem button className={classes.root}
                      onClick={(event => handleActivateModal(event))}>
                <ListItemIcon className={classes.root}>
                    <AddCircleIcon  className={classes.addIcon}/>
                </ListItemIcon>
                <ListItemText primary="Добавить счет" />
            </ListItem>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className={classes.paper}>
                        <h4 id="transition-modal-title">Добавить счет</h4>
                        <div id="transition-modal-description-input">
                            <TextField
                                className={classes.input}
                                id="outlined-basic"
                                label="Название счета"
                                variant="outlined"
                                value={billValue}
                                onChange={(event)=>handleChangeBillValue(event)}
                            />
                        </div>
                        <div id="transition-modal-description">
                            <Button
                                onClick={handleClose}
                                variant="contained">Отмена</Button>
                            <Button
                                onClick={(event => handleAddBill(event))}
                                variant="contained"
                                color="primary"
                            >
                                Сохранить
                            </Button>
                        </div>
                    </div>
                </Fade>
            </Modal>
        </div>
    )
}