import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import IncomeTab from "../income-tab";
import yellow from "@material-ui/core/colors/yellow";
import brown from "@material-ui/core/colors/brown";
import OutcomeTab from "../outcome-tab";
import './tabs-plus.scss'

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography component={'div'}>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `full-width-tab-${index}`,
        'aria-controls': `full-width-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: theme.palette.background.paper,
        width: 600,
        [theme.breakpoints.down('xs')]: {
            width: 430,
        },
        [theme.breakpoints.down(500)]: {
            width: 300,
        },
    },
    paper: {
        flexGrow: 1,
        backgroundColor: yellow[200],
        color: brown[900],
    }
}));

export default function TabsPlus({handleClose}) {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <div className={classes.root}>
            <div className="tabs-plus">
                <AppBar position="static">
                    <Tabs className={classes.paper} value={value} onChange={handleChange} indicatorColor="primary"
                          variant="fullWidth" aria-label="full width tabs example">
                        <Tab style={{fontSize: 18}} label="Доход" {...a11yProps(0)} />
                        <Tab style={{fontSize: 18}} label="Расход" {...a11yProps(1)} />
                    </Tabs>
                </AppBar>
                <TabPanel value={value} index={0}>
                    <IncomeTab handleClose={handleClose}/>
                </TabPanel>
                <TabPanel value={value} index={1}>
                    <OutcomeTab handleClose={handleClose}/>
                </TabPanel>
            </div>
        </div>
    );
}