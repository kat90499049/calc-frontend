import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import { useSelector} from "react-redux";
import AddBills from "../add-bills";
import {yellow} from "@material-ui/core/colors";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    common: {
        backgroundColor: yellow[600]
    }
}));


export default function MoneyList() {

    const classes = useStyles();

    const userLoading = useSelector((state) => state.registrationReducer.user);


    return (
        <div className={classes.root}>
            <Grid>
                <List style={{padding: 0}} component="nav" aria-label="main mailbox folders">
                    {
                        userLoading?.sum
                            ?
                            <Box>
                                <ListItem button
                                          className={classes.common}
                                >
                                    <ListItemText primary="Общая сумма денежных средств" />
                                    <Box display="flex" justifyContent="flex-end">
                                        <ListItemText edge="end" primary={userLoading?.sum} />
                                    </Box>
                                </ListItem>
                                <Divider/>
                            </Box>
                            : null
                    }
                    {
                        userLoading?.bills ?
                            userLoading?.bills.map((bill)=> {
                                return (
                                    <Box key={bill.id}>
                                        <ListItem button>
                                            <ListItemText primary={bill.name} />
                                            <Box display="flex" justifyContent="flex-end">
                                                <ListItemText edge="end" primary={bill.sum} />
                                            </Box>
                                        </ListItem>
                                        <Divider/>
                                    </Box>
                                )
                            })
                            :null
                    }
                        <AddBills/>
                </List>
            </Grid>
            <Divider />
        </div>
    );
}