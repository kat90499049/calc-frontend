import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {logoutUser} from "../../actions/user-actions";
import {userDetail} from "../../services/service";
import {Link} from "react-router-dom";
import BackdropSpinner from "../backdrop-spinner";
import './common-info.scss';
import { Tab } from 'semantic-ui-react';
import OutcomeList from "../outcome-list";
import CommonMain from "../common-main";

const panes = [
    {
        menuItem: 'Главная',
        render: () => <Tab.Pane attached={false}><CommonMain/></Tab.Pane>,
    },
    {
        menuItem: 'Доходы',
        render: () => <Tab.Pane attached={false}>Tab 2 Content</Tab.Pane>,
    },
    {
        menuItem: 'Расходы',
        render: () => <Tab.Pane attached={false}><OutcomeList/></Tab.Pane>,
    },
];

export default function CommonInfo () {

    const dispatch = useDispatch();

    const user = useSelector((state) => state.registrationReducer.user);

    const loading = useSelector((state)=> state.userReducer.loading);

    useEffect(() => {
        dispatch(userDetail())
    }, [dispatch]);

    const handleClick = event => {
        event.preventDefault();
        localStorage.removeItem("token");
        dispatch(logoutUser());
    };

    return (
        <div className="common-info container">
            <div className="row common-info_header">
                {user?.first_name
                    ?
                    <div className="header_user">
                        <h5>Привет, {user?.first_name}!</h5>
                        <button onClick={(event) =>handleClick(event)}><Link to="/">Выход</Link></button>
                    </div>
                    : <div className="spinner-border" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                }
                <div className="header_label">
                    <div className="row">
                        <img src="/images/logo.png" alt="Counting House"/>
                    </div>
                    <div className="row">
                        <h3>Домашняя онлайн бухгалтерия</h3>
                    </div>
                </div>
            </div>

            <Tab menu={{color: 'yellow', inverted: true, attached: false, tabular: false }} panes={panes} />

            <div className="row common-info_footer">
                <h5>
                    &#169; 2020 Counting House.
                </h5>
            </div>
            {loading ? <BackdropSpinner/>: null}
        </div>
    )
};