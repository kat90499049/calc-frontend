import React, {useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import Box from "@material-ui/core/Box";
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import TabsPlus from "../tabs-plus";

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',

    },
    paper: {
        minWidth: 300,
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(1, 1, 1),
        [theme.breakpoints.down('xs')]: {
            width: 450
        },
        [theme.breakpoints.down(500)]: {
            width: 320
        },
    },
    close: {
        display: 'flex',
        justifyContent: 'flex-end',
        [theme.breakpoints.down('xs')]: {
            width: 430
        },
        [theme.breakpoints.down(500)]: {
            width: 300
        },
    },
}));

export default function ModalPlus({openModalPlus, setOpenModalPlus}) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    useEffect(() => {
        if(openModalPlus){
            handleOpen();
        }
    }, [openModalPlus]);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
        setOpenModalPlus(false);
    };

    return (
        <div >
            {/*<Button type="button" onClick={handleOpen}>*/}
            {/*    <AddCircleIcon style={{ color:green[500], fontSize: 70 }}/>*/}
            {/*</Button>*/}
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <Box className={classes.paper}>
                        {/*<h2 id="transition-modal-title">Transition modal</h2>*/}
                        {/*<p id="transition-modal-description">react-transition-group animates me.</p>*/}
                        <div className={classes.close}>
                            <IconButton
                                aria-label="close"
                                size="small"
                                onClick={handleClose}
                            >
                                <CloseIcon fontSize="small"/>
                            </IconButton>
                        </div>
                        <TabsPlus handleClose={handleClose}/>
                    </Box>
                </Fade>
            </Modal>
        </div>
    );
}