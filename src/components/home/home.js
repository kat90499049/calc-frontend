import React, {useEffect, useState} from "react";

import "./home.scss";
import {useDispatch, useSelector} from "react-redux";
import {logoutUser} from "../../actions/user-actions";
import {userDetail} from "../../services/service";
import {Link} from "react-router-dom";
import MoneyList from "../money-list";
import OutcomeList from "../outcome-list";
import ModalPlus from "../modal-plus";
import BackdropSpinner from "../backdrop-spinner";
import ModalMinus from "../modal-minus";
import {green, red} from "@material-ui/core/colors";
import Button from '@material-ui/core/Button';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';

const Home = () => {

    const dispatch = useDispatch();

    const user = useSelector((state) => state.registrationReducer.user);

    const loading = useSelector((state)=> state.userReducer.loading);

    const [openModalPlus, setOpenModalPlus] = useState(false);

    const [openModalMinus, setOpenModalMinus] = useState(false);

    useEffect(() => {
        dispatch(userDetail())
    }, [dispatch]);

    const handleClick = event => {
        event.preventDefault();
        localStorage.removeItem("token");
        dispatch(logoutUser());
    };

    const handleOpenModalPlus = (e)=> {
        e.preventDefault();
        setOpenModalPlus(true);
    };

    const handleOpenModalMinus = (e)=> {
        e.preventDefault();
        setOpenModalMinus(true);
    };

    return (
        <div className="home container">
            <div className="row home_header">
                {user?.first_name
                    ?
                    <div className="header_user">
                        <h5>Привет, {user?.first_name}!</h5>
                        <button onClick={(event) =>handleClick(event)}><Link to="/">Выход</Link></button>
                    </div>
                    : <div className="spinner-border" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                }
                <div className="header_label">
                    <div className="row">
                        <img src="/images/logo.png" alt="Counting House"/>
                    </div>
                    <div className="row">
                        <h3>Домашняя онлайн бухгалтерия</h3>
                    </div>
                </div>
            </div>
            <div className="home_main row">
                <div className="col-md">
                    <div className="plus-minus">
                        <div className="plus">
                            <h3>Доход</h3>
                            {/*<Button>*/}
                            {/*    <AddCircleIcon style={{ color:green[500], fontSize: 70 }}/>*/}
                            {/*</Button>*/}
                            <Button type="button" onClick={handleOpenModalPlus}>
                                <AddCircleIcon style={{ color:green[500], fontSize: 70 }}/>
                            </Button>
                            <ModalPlus
                                openModalPlus={openModalPlus}
                                setOpenModalPlus={setOpenModalPlus}
                            />
                        </div>
                        <div className="minus">
                            <h3>Расход</h3>
                            <Button type="button" onClick={handleOpenModalMinus}>
                                <RemoveCircleIcon style={{color:red[500], fontSize: 70 }}/>
                            </Button>
                            <ModalMinus
                                openModalMinus={openModalMinus}
                                setOpenModalMinus={setOpenModalMinus}
                            />
                        </div>
                    </div>
                    <div className="money">
                        <MoneyList/>
                    </div>
                </div>
                <div className="col-md costs">
                   <OutcomeList/>
                </div>

            </div>
            <div className="row home_footer">
                <h5>
                    &#169; 2020 Counting House.
                </h5>
            </div>

            {loading ? <BackdropSpinner/>: null}
        </div>
    )
};

export default Home;