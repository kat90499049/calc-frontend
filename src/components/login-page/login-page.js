import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";

import "./login-page.scss";
import {loginUser} from "../../services/service";
import BackdropSpinner from "../backdrop-spinner";


const LoginPage = () => {

    const dispatch = useDispatch();

    const [user, setUser] = useState({
        email: '',
        password: ''
    });

    const [submitted, setSubmitted] = useState(false);

    const loading = useSelector((state) => state.authenticationReducer.loading);

    const error = useSelector((state) => state.authenticationReducer.error);

    const handleChange = (e) => {
        const {name, value} = e.target;
        setUser({
            ...user,
            [name]: value
        })
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        setSubmitted(true);
        if (!(user.email && user.password)) {
            return;
        }
        dispatch(loginUser(user.email, user.password))
    };

    return (
        <div className="login-page container">
            <div className="row ">
                <div className="login-page_label col-lg-4 col-md-12 ">
                    <div className="shadow-lg p-3 mb-5 bg-white rounded">
                        <img src="/images/logo.png" alt="Counting House"/>
                    </div>

                </div>
                <div className="login-page_form col-lg col-md-12 ">
                    <h2>Вход в аккаунт</h2>
                    <form name="form">
                        <div className={'form-group' + (submitted && !user.email ? ' has-error' : '')}>
                            <label htmlFor="email">E-mail</label>
                            <input type="email" className="form-control" name="email" value={user.email} onChange={(event)=>handleChange(event)} />
                            {submitted && !user.email &&
                            <div className="help-block">Необходимо заполнить "E-mail"</div>
                            }
                        </div>
                        <div className={'form-group' + (submitted && !user.password ? ' has-error' : '')}>
                            <label htmlFor="password">Пароль</label>
                            <input type="password" className="form-control" name="password" value={user.password} onChange={(event)=>handleChange(event)} />
                            {submitted && !user.password &&
                            <div className="help-block">Необходимо заполнить "Пароль"</div>
                            }
                        </div>
                        <div className="form-group form_buttons ">
                            <button type="button" className="btn btn-lg btn-warning"
                                    onClick={(event)=>handleSubmit(event)}>Вход в аккаунт</button>
                            {/*{loading ?*/}
                            {/*        <div className="spinner-border" role="status">*/}
                            {/*                <span className="sr-only">Loading...</span>*/}
                            {/*        </div> :*/}
                            {/*        <div className="spinner-border-null" role="status">*/}
                            {/*        <span className="sr-only">Loading...</span>*/}
                            {/*        </div>*/}
                            {/*}*/}
                            {loading ? <BackdropSpinner/>: null}
                            <button type="button" className="btn btn-lg btn-warning" ><Link to="/">Отмена</Link></button>
                        </div>
                        <div className="form-group form-link">
                            <Link to="/registration">Регистрация</Link>
                            <i className="fa fa-long-arrow-right"></i>
                        </div>
                        {error &&
                        <div className="alert alert-danger" role="alert">
                            {error}
                        </div>
                        }
                    </form>
                </div>
            </div>
        </div>
    )
};

export default LoginPage;