import React, {Component} from "react";
import {Router,Route, Switch} from 'react-router-dom';
import {connect} from 'react-redux';

import {history} from "../../history";
import {alertActions} from "../../actions";
import HomePage from "../home-page";
import Home from "../home";
import RegisterPage from "../register-page";
import LoginPage from "../login-page";
import CommonInfo from "../common-info";

import './app.scss';

class App extends Component {

    constructor(props) {
        super(props);

        history.listen((location, action) => {
            this.props.clearAlerts();
        })
    }

    render() {

        const { alert} = this.props;
        return (
            <div className="app">
                <div>
                    {alert.message &&
                    <div className={`alert ${alert.type}`}>{alert.message}</div> }
                    <Router history={history}>
                        <Switch>
                            <Route
                                path="/"
                                component={HomePage}
                                exact/>
                            <Route
                                path="/login"
                                component={LoginPage}/>
                            <Route
                                path="/registration"
                                component={RegisterPage}/>
                            <Route
                                path="/home"
                                component={Home}/>
                            <Route
                                path="/commoninfo"
                                component={CommonInfo}/>
                        </Switch>
                </Router>
                </div>
            </div>
        )
    }

};

const mapStateToProps = (state)=> {
    const {alert} = state;
    return{alert}
};

const actionCreators = {
    clearAlerts: alertActions.clear
};

export default connect(mapStateToProps,actionCreators)(App);